﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T obj)
        {
            if (obj.Id.Equals(Guid.Empty))
                obj.Id = Guid.NewGuid();

            Data = Data.Concat(new[] { obj });
            return Task.FromResult(Data.LastOrDefault());
        }

        public Task<T> UpdateAsync(T obj)
        {
            var data = Data.ToList();
            for ( int i = 0; i < Data.Count(); i++)
            {
                if (data[i].Id.Equals(obj.Id))
                {
                    data[i] = obj;
                }
            }

            Data = data;

            return Task.FromResult(obj);
        }

        public async Task<bool> DeleteAsync(Guid id)
        {
            var list = Data.Where(i => !i.Id.Equals(id)).ToList();
            Data = list;

            return true;
        }
    }
}